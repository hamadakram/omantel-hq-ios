//
//  FloorPlanViewController.swift
//  Omantel HQ
//
//  Created by hammad.akram on 21/03/2019.
//  Copyright © 2019 hammad.akram. All rights reserved.
//

import UIKit

class FloorPlanViewController: UIViewController {

    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let screenWidth = UIScreen.main.bounds.size.width
        imageHeight.constant = CGFloat(screenWidth * 1.777)
    }
    
    @IBAction func backAction(_ sender: Any) {
         navigationController?.popViewController(animated: true)
    }
    
}
