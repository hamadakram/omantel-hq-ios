//
//  FeatureCollectionViewCell.swift
//  Omantel HQ
//
//  Created by hammad.akram on 28/02/2019.
//  Copyright © 2019 hammad.akram. All rights reserved.
//

import UIKit

class FeatureCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    func configure(feature: Feature) {
        contentView.layer.cornerRadius = 4
        contentView.layer.masksToBounds = true
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.2
        layer.masksToBounds = false
        
        // Set data
        
        icon.image = feature.icon
        title.text = feature.title
    }
}


