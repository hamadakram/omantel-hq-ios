//
//  FeaturesViewController.swift
//  Omantel HQ
//
//  Created by hammad.akram on 28/02/2019.
//  Copyright © 2019 hammad.akram. All rights reserved.
//

import UIKit

class FeaturesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var featureCollectionView: UICollectionView!
    
    let features = [
        Feature(title: "hot_desking".localized(), icon: UIImage(named: "Hot Desking")!),
        Feature(title: "digitalisation".localized(), icon: UIImage(named: "Digitalisation")!),
        Feature(title: "restroom".localized(), icon: UIImage(named: "Restroom")!),
        Feature(title: "canteen".localized(), icon: UIImage(named: "Canteen")!),
        Feature(title: "auditorium".localized(), icon: UIImage(named: "Auditorium")!),
        Feature(title: "others".localized(), icon: UIImage(named: "Others")!)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.featureCollectionView.dataSource = self
        self.featureCollectionView.delegate = self
        
        let flow = featureCollectionView.collectionViewLayout as! UICollectionViewFlowLayout // If you create collectionView programmatically then just create this flow by UICollectionViewFlowLayout() and init a collectionView by this flow.
        
        let itemSpacing: CGFloat = 20
        let itemsInOneLine: CGFloat = 2
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1) - 64
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        flow.minimumInteritemSpacing = itemSpacing
        flow.minimumLineSpacing = itemSpacing

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeatureCollectionViewCell", for: indexPath) as! FeatureCollectionViewCell
        cell.configure(feature: features[indexPath.row])
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            pushContentViewController(contentType: "hot_desking")
            break
        case 1:
            pushContentViewController(contentType: "digitalization")
            break
        case 2:
            pushContentViewController(contentType: "restroom")
            break
        case 3:
            pushContentViewController(contentType: "canteen")
            break
        case 4:
            pushContentViewController(contentType: "auditorium")
            break
        case 5:
            pushContentViewController(contentType: "others")
            break
        default:
            break
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets (top: 10, left: 32, bottom: 10, right: 32)
    }
    
    func pushContentViewController(contentType: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContentViewController") as! ContentViewController
        controller.contentType = contentType
        navigationController?.pushViewController(controller, animated: true)
    }

    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

class Feature: NSObject {
    var title: String = ""
    var icon: UIImage
    init(title: String, icon: UIImage) {
        self.title = title
        self.icon = icon
    }
    
}
