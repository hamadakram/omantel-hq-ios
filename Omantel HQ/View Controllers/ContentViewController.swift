//
//  ContentViewController.swift
//  Omantel HQ
//
//  Created by hammad.akram on 26/02/2019.
//  Copyright © 2019 hammad.akram. All rights reserved.
//

import UIKit
import Localize_Swift

class ContentViewController: UIViewController {
    
    @IBOutlet weak var contentText: UITextView!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    var contentType: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch contentType {
        case "ceo":
            self.pageTitle.text = "ceo_message".localized()
            icon.image = UIImage(named: "Quotation Mark")
            setText(textView: contentText, text: "ceo_message_content".localized())
            break
        case "cpo":
            self.pageTitle.text = "cpo_message".localized()
            icon.image = UIImage(named: "Quotation Mark")
            setText(textView: contentText, text: "cpo_message_content".localized())
            break
        case "feature":
            self.pageTitle.text = "feature".localized()
            icon.image = UIImage(named: "Features Icon Stroke")
            setText(textView: contentText, text: "topography_content".localized())
            break
        case "location":
            self.pageTitle.text = "location".localized()
            icon.image = UIImage(named: "Location Icon Stroke")
            setText(textView: contentText, text: "location_content".localized())
            break
        case "topography":
            self.pageTitle.text = "topography".localized()
            icon.image = UIImage(named: "Topography Icon Stroke")
            setText(textView: contentText, text: "topography_content".localized())
            break
        case "external":
            self.pageTitle.text = "external_view".localized()
            icon.image = UIImage(named: "External Views Stroke")
            setText(textView: contentText, text: "external_content".localized())
            break
        case "hot_desking":
            self.pageTitle.text = "hot_desking".localized()
            icon.image = UIImage(named: "Hot Desking Stroke")
            icon.alpha = 0.25
            setText(textView: contentText, text: "hot_desking_content".localized())
            break
        case "digitalization":
            self.pageTitle.text = "digitalisation".localized()
            icon.image = UIImage(named: "Digitalisation Stroke")
            icon.alpha = 0.25
            setText(textView: contentText, text: "digitalisation_content".localized())
            break
        case "restroom":
            self.pageTitle.text = "restroom".localized()
            icon.image = UIImage(named: "Restroom Stroke")
            icon.alpha = 0.25
            setText(textView: contentText, text: "restroom_content".localized())
            break
        case "canteen":
            self.pageTitle.text = "canteen".localized()
            icon.image = UIImage(named: "Canteen Stroke")
            icon.alpha = 0.25
            setText(textView: contentText, text: "canteen_content".localized())
            break
        case "auditorium":
            self.pageTitle.text = "auditorium".localized()
            icon.image = UIImage(named: "Auditorium Stroke")
            icon.alpha = 0.25
            setText(textView: contentText, text: "auditorium_content".localized())
            break
        case "others":
            self.pageTitle.text = "others".localized()
            icon.image = UIImage(named: "Others Stroke")
            icon.alpha = 0.25
            setText(textView: contentText, text: "others_content".localized())
            break
        case "dosanddonts":
            self.pageTitle.text = "dosanddonts".localized()
            icon.image = nil
            setText(textView: contentText, text: "dosanddonts_content".localized())
            break
        default:
            ""
            break
        }
    }
    
    func setText(textView: UITextView, text: String) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 6
        paragraphStyle.paragraphSpacing = 20
        if(Localize.currentLanguage() == "ar"){
            paragraphStyle.alignment = .right
        }
        
        
        
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
                          NSAttributedString.Key.foregroundColor: UIColor.blueyGrey,
                          NSAttributedString.Key.paragraphStyle: paragraphStyle]
        
        contentText.attributedText = text.htmlAttributedString
        
        for attribute in attributes {
            contentText.textStorage.addAttribute(attribute.key, value: attribute.value, range: NSRange.init(location: 0, length: contentText.textStorage.length))
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
