//
//  HomeViewController.swift
//  Omantel HQ
//
//  Created by hammad.akram on 25/02/2019.
//  Copyright © 2019 hammad.akram. All rights reserved.
//

import UIKit
import AVKit
import Localize_Swift

class HomeViewController: UIViewController {
    
    @IBOutlet weak var videoScroll: UIScrollView!
    @IBOutlet weak var exploreScrollView: UIScrollView!
    
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var videoOneLabel: UILabel!
    @IBOutlet weak var videoTwoLabel: UILabel!    
    @IBOutlet weak var ceoLabel: UILabel!
    @IBOutlet weak var cpoLabel: UILabel!
    @IBOutlet weak var featureLabel: UILabel!
    @IBOutlet weak var exploreLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var topographyLabel: UILabel!
    @IBOutlet weak var externalLabel: UILabel!
    @IBOutlet weak var nextIconCpo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(Localize.currentLanguage() != "ar"){
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            videoScroll.scrollToEnd(scrollSize: 654)
            exploreScrollView.scrollToEnd(scrollSize: 632)
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        setUpView()
    }

    func setUpView() {
        homeLabel.text = "home".localized()
        videoOneLabel.text = "video_one_title".localized()
        videoTwoLabel.text = "video_two_title".localized()
        ceoLabel.text = "ceo_message".localized()
        cpoLabel.text = "cpo_message".localized()
        featureLabel.text = "feature".localized()
        exploreLabel.text = "explore".localized()
        locationLabel.text = "location".localized()
        topographyLabel.text = "topography".localized()
        externalLabel.text = "external_view".localized()
        
        nextIconCpo.image = UIImage(named: "Next Icon")?.imageFlippedForRightToLeftLayoutDirection()

    }
    @IBAction func languageAction(_ sender: Any) {
        if (self.navigationController != nil) {
            for vc in  self.navigationController!.viewControllers {
                if vc is LanguageViewController {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
    }
    
    @IBAction func ceoMessageAction(_ sender: Any) {
        pushContentViewController(contentType: "ceo")
    }
    
    @IBAction func cpoMessageAction(_ sender: Any) {
        pushContentViewController(contentType: "cpo")
    }
    
    @IBAction func featureAction(_ sender: Any) {
        push(storyBoardId: "FeaturesViewController")
    }
    
    @IBAction func locationAction(_ sender: Any) {
        pushContentViewController(contentType: "location")
    }
    
    @IBAction func topographyAction(_ sender: Any) {
        pushContentViewController(contentType: "topography")
    }
    
    @IBAction func externalAction(_ sender: Any) {
        pushContentViewController(contentType: "external")
    }
    
    
    @IBAction func videoOneAction(_ sender: Any) {
        playVideo(videoUrl: "http://omantel.indigo-oman.com/video/OmantelHQ.mp4")
    }
    
    @IBAction func videoTwoAction(_ sender: Any) {
        playVideo(videoUrl: "http://omantel.indigo-oman.com/video/OmantelHQ-English.mp4")
    }
    
    @IBAction func floorDistributionAction(_ sender: Any) {
        push(storyBoardId: "FloorPlanViewController")
    }
    
    @IBAction func DosDontsAction(_ sender: Any) {
        pushContentViewController(contentType: "dosanddonts")
    }
    
    func pushContentViewController(contentType: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ContentViewController") as! ContentViewController
        controller.contentType = contentType
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func playVideo(videoUrl: String) {
        let player = AVPlayer(url: NSURL(string: videoUrl)! as URL)
        
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        present(playerViewController, animated: true) {
            player.play()
        }
    }
}

extension UIScrollView {
    
    func scrollToEnd(scrollSize: Int) {
        //let bottomOffset = CGPoint(x: contentSize.width - bounds.size.width + contentInset.left, y: 0)
        let bottomOffset = CGPoint(x: scrollSize, y: 0)
        setContentOffset(bottomOffset, animated: true)
    }
}
