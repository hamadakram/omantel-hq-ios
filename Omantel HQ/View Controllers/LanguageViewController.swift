//
//  ViewController.swift
//  Omantel HQ
//
//  Created by hammad.akram on 25/02/2019.
//  Copyright © 2019 hammad.akram. All rights reserved.
//

import UIKit
import Localize_Swift

class LanguageViewController: UIViewController {

    @IBOutlet weak var btArabic: UIButton!
    @IBOutlet weak var btEnglish: UIButton!
    
    @IBOutlet weak var selectLanguage: UILabel!
    @IBOutlet weak var languageDetails: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    var isEnglish = false
    var language = "en"
    
    
    override func viewDidLoad() {
        
        if(Localize.currentLanguage() != "ar"){
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        let isLanguageSelected = UserDefaults.standard.bool(forKey: "LanguageSelected")
        
        if(isLanguageSelected){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            navigationController?.pushViewController(controller, animated: true)
        }else{
            super.viewDidLoad()
            englishAction(btEnglish)
        }
    }

    
    @objc func setUpView(){
        if(language == "en"){
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else{
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        updateView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
    }

    @IBAction func arabicAction(_ sender: UIButton) {
        //self.isEnglish = false
        //selectLanguage(language: "ar")
        self.showAlert()
    }
    
    @IBAction func englishAction(_ sender: UIButton) {
        self.isEnglish = true
        selectLanguage(language: "en")
    }
    
    func selectLanguage(language: String) {
        self.language = language
        switch language {
        case "ar":
            makeSelected(button: btArabic)
            makeUnSelected(button: btEnglish)
        case "en":
            makeSelected(button: btEnglish)
            makeUnSelected(button: btArabic)
        default:
            ""
        }
    }
    
    func makeSelected(button: UIButton) {
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.darkishBlue.cgColor
    }
    
    func makeUnSelected(button: UIButton) {
        button.layer.borderWidth = 0.0
        button.layer.borderColor = UIColor.clear.cgColor
    }
    @IBAction func doneAction(_ sender: Any) {
        
        changeAppLanguage(language: language)
        
        UserDefaults.standard.set(true, forKey: "LanguageSelected")

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        navigationController?.pushViewController(controller, animated: true)
    }
    func changeAppLanguage(language: String) {
        Localize.setCurrentLanguage(language)
    }
    
    func updateView() {
        selectLanguage.text = "select_language".localized()
        languageDetails.text = "select_language_details".localized()
        doneButton.setTitle("done".localized(), for: .normal)

    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Coming soon", message: "Arabic language will be available soon.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
           }))
        self.present(alert, animated: true, completion: nil)
    }
}

