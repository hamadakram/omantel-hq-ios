//
//  UIViewController+Additions.swift
//  Omantel HQ
//
//  Created by hammad.akram on 25/02/2019.
//  Copyright © 2019 hammad.akram. All rights reserved.
//

import UIKit
import Localize_Swift

extension UIViewController {
    
    public func push(storyBoardId: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: storyBoardId)
        navigationController?.pushViewController(controller, animated: true)
    }
    public func pop(storyBoardId: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: storyBoardId)
        navigationController?.popToViewController(controller, animated: true)
    }
    
}
