//
//  UIColor+Additions.swift
//  Omantel HQ
//
//  Generated on Zeplin. (2/25/2019).
//  Copyright (c) 2019 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIColor {

  @nonobjc class var darkishBlue: UIColor {
    return UIColor(red: 0.0, green: 74.0 / 255.0, blue: 151.0 / 255.0, alpha: 1.0)
  }
    @nonobjc class var blueyGrey: UIColor {
        return UIColor(red: 140.0 / 255.0, green: 154.0 / 255.0, blue: 172.0 / 255.0, alpha: 1.0)
    }

}
