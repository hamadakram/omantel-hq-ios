//
//  String+Additions.swift
//  Omantel HQ
//
//  Created by hammad.akram on 26/02/2019.
//  Copyright © 2019 hammad.akram. All rights reserved.
//

import UIKit

extension String {
//    var htmlToAttributedString: NSAttributedString? {
//        guard let data = data(using: .utf8) else { return NSAttributedString() }
//        do {
//            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
//        } catch {
//            return NSAttributedString()
//        }
//    }
    var htmlAttributedString: NSAttributedString? {
        
        do {
            return try NSAttributedString(data: data(using: .utf8)!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error {
            print(error.localizedDescription)
        }
        return nil
    }
   
}
