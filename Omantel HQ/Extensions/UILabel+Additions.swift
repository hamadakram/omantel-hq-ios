//
//  UILabel+Additions.swift
//  Omantel HQ
//
//  Created by hammad.akram on 25/02/2019.
//  Copyright © 2019 hammad.akram. All rights reserved.
//

import UIKit

//protocol Localizable {
//    var localize: String { get }
//}
//extension String: Localizable {
//    var localize: String {
//        return NSLocalizedString(self, comment: "")
//    }
//}
//protocol XIBLocalizable {
//    var xibLocKey: String? { get set }
//}
//
//extension UILabel: XIBLocalizable {
//    @IBInspectable var xibLocKey: String? {
//        get { return nil }
//        set(key) {
//            text = key?.localize
//        }
//    }
//}
//extension UIButton: XIBLocalizable {
//    @IBInspectable var xibLocKey: String? {
//        get { return nil }
//        set(key) {
//            setTitle(key?.localize, for: .normal)
//        }
//    }
//}
