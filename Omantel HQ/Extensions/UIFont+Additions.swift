//
//  UIFont+Additions.swift
//  Omantel HQ
//
//  Generated on Zeplin. (2/25/2019).
//  Copyright (c) 2019 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIFont {

  class var textStyle: UIFont {
    return UIFont(name: "Roboto-Bold", size: 20.0)!
  }

}
